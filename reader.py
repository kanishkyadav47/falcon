import pandas as pd
import ast

# Reading tables' and columns' names
file = open('tables_columnsName.txt', 'r')
Lines = file.readlines()

conjugate_list = []
# Strip the newline character
for line in Lines:
    if line != '\n':
        conjugate_list.append(line.strip('\n'))


print(conjugate_list)
print(conjugate_list.__len__())

# Initiating dataframe that from the table that has the highest number of rows
# Setting the first row of Counrty names as index

df0 = pd.read_html('data/economic_unemployment.xls')
index0 = df0[0].loc[:,('Unnamed: 0_level_0', 'Unnamed: 0_level_1')]
data = df0[0].loc[:,('Unemployment, male (% of male labor force) (modeled ILO estimate)', '2018')].tolist()

# Print number of rows of the initiated dataframe
print(len(index0))

frame = pd.DataFrame({'Country':index0, "('Unemployment, male (% of male labor force) (modeled ILO estimate)', '2018')":data})
frame = frame.set_index('Country')
print(frame)

for i in conjugate_list:
    res = i.split('^')
    df = pd.read_html(res[0])
    # Create Pandas series object with index as countries.
    s = pd.Series(df[0].loc[:,ast.literal_eval(res[1])].tolist(), index=df[0].iloc[:,0].tolist())
    # Series created using pandas series object  as list. Else was giving Nan values
    # print(s)
    # print(len(s)) # Checking longest column to use its index as index for all. economic_unemployment -> 264 rows. Modified code above to vreate main dataframe with index as countries

    frame[res[1]] = s # add series for each table into main dataframe

print(frame)

frame.to_csv('./'+'my_df.csv')