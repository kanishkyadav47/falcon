import pandas as pd


df = pd.read_csv('./'+'my_df.csv')
df = df.set_index('Country')
print(df.head())

countries = df.index.tolist()
naList = [] # list of countries that has missing values

# Creating a list of the countries that has missing values
for i in range(len(df.index)) :
    if df.iloc[i].isnull().sum() != 0:
        print(countries[i], " : ", df.iloc[i].isnull().sum())
        naList.append(i)

initial_num_of_rows = len(df.index)

# Most of the countries that have NA values have missing values for most of the columns.
# 34 and 30 out of 36 columns

# removing those countries from the data frame
for i in naList:
    df = df.drop(index=countries[i])

df_removedNA = df
print('\nNumber of columns removed: ', initial_num_of_rows-len(df_removedNA.index))
df_removedNA.to_csv('./'+'my_df_removedNA.csv')

